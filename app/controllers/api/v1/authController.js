/**
 * @file contains authentication request handler and its business logic
 * @author Fikri Rahmat Nurhidayat
 */

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { User } = require("../../../models");
const SALT = 10;

function encryptPassword(password) {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, SALT, (err, encryptedPassword) => {
      if (!!err) {
        reject(err);
        return;
      }

      resolve(encryptedPassword);
    });
  });
}

function checkPassword(encryptedPassword, password) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, encryptedPassword, (err, isPasswordCorrect) => {
      if (!!err) {
        reject(err);
        return;
      }

      resolve(isPasswordCorrect);
    });
  });
}

function createToken(payload) {
  return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || "Rahasia");
}

module.exports = {
  async register(req, res) {
    const email = req.body.email;
    const encryptedPassword = await encryptPassword(req.body.password);
    const user = await User.create({ email, encryptedPassword });
    res.status(201).json({
      id: user.id,
      email: user.email,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    });
  },

  async login(req, res) {
    const email = req.body.email.toLowerCase(); // Biar case insensitive
    const password = req.body.password;

    const user = await User.findOne({
      where: { email },
    });

    if (!user) {
      res.status(404).json({ message: "Email tidak ditemukan" });
      return;
    }

    const isPasswordCorrect = await checkPassword(user.encryptedPassword, password);

    if (!isPasswordCorrect) {
      res.status(401).json({ message: "Password salah!" });
      return;
    }

    const token = createToken({
      id: user.id,
      password: user.encryptPassword,
      email: user.email,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    });

    res.status(201).json({
      id: user.id,
      email: user.email,
      token,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    });
  },

  async whoAmI(req, res) {
    res.status(200).json(req.user);
  },

  async authorize(req, res, next) {
    console.log("req.headers :");
    console.log(req.headers);
    // req.headers :
    // {
    //   authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJ5dW5lZGFAZ21haWwuY29tIiwiY3JlYXRlZEF0IjoiMjAyMi0wNS0wOFQxMzowNjo1My4xMTZaIiwidXBkYXRlZEF0IjoiMjAyMi0wNS0wOFQxMzowNjo1My4xMTZaIiwiaWF0IjoxNjUyMDE1NzMyfQ.BYhazU9e8j2dbTP6McwoRC0_Wro4QJjeFeuqRF5y3rM',
    //   'user-agent': 'PostmanRuntime/7.29.0',
    //   accept: '*/*',
    //   'postman-token': '46033065-65df-47d4-a735-5dc6242bb29a',
    //   host: 'localhost:8000',
    //   'accept-encoding': 'gzip, deflate, br',
    //   connection: 'keep-alive'
    // }
    try {
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      const tokenPayload = jwt.verify(token, process.env.JWT_SIGNATURE_KEY || "Rahasia");
      console.log("token payload :");
      console.log(tokenPayload);
      // token payload :
      // {
      //   id: 1,
      //   email: 'yuneda@gmail.com',
      //   createdAt: '2022-05-08T13:06:53.116Z',
      //   updatedAt: '2022-05-08T13:06:53.116Z',
      //   iat: 1652015732
      // }

      req.user = await User.findByPk(tokenPayload.id);
      next();
    } catch (err) {
      console.error(err);
      res.status(401).json({
        message: "Unauthorized",
      });
    }
  },
};
